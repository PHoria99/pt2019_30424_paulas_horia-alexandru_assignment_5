import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Handling {
	private ArrayList<MonitoredData> data;
	private HashMap<String, Long> timeTakenTable;
	private ArrayList<String> timeTakenKey;
	private HashMap<String, Integer> map;
	
	public Handling()
	{
		data = new ArrayList<>();
	}
	
	public void everything() throws IOException
	{
		data = read();
		countDays();
		mapActivities();
		countActivityAppearances();
		activityDuration();
		totalActivityDuration();
		filter();
	}
	
	private static ArrayList<MonitoredData> read()
	{
		ArrayList<MonitoredData> md = new ArrayList<>();
		String filename = "./Activities.txt";
		
		try(Stream<String> stream = Files.lines(Paths.get(filename)))
		{
			stream.forEach(s -> md.add(new MonitoredData(s.substring(0, 19),s.substring(21, 39), s.substring(42))));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
		return md;
	}
	
	private void countDays() throws IOException
	{
		int days = 0;
		List<String> dateList = new ArrayList<>();
	
		data.stream().forEach(p -> dateList.add(p.getDate()));
		days = (int) dateList.stream().distinct().count();
		
		
		
		String filename = "days.txt";
		File logFile = new File(filename);
		BufferedWriter writer = new BufferedWriter(new FileWriter(logFile));
		writer.write(days + "");
		writer.close();
	}
	
	private void mapActivities() throws IOException
	{
		map = new HashMap<>();
		List<String> activities = new ArrayList<>();
		List<String> activityList = new ArrayList<>();
		//extract get all activities
		data.stream().forEach(p -> activities.add(p.activity));
		//create a list of distinct activities
		activityList = activities.stream().distinct().collect(Collectors.toList());
		//For each activity in activityList, filter entries from data, count them and put them into the map
		activityList.forEach(a -> map.put(a, (int)data.stream().filter(p-> p.activity.equals(a)).count()));
		
		String filename = "activities.txt";
		File logFile = new File(filename);
		BufferedWriter writer = new BufferedWriter(new FileWriter(logFile));

		for(String s:activityList)
		{
			writer.write(s + " " + map.get(s) + "\n");
		}
		
		writer.close();
		
	}
	
	private void countActivityAppearances() throws IOException{
		List<String> dates = new ArrayList<>();
		List<String> activities = new ArrayList<>();
		//extract all dates
		data.stream().forEach(p -> dates.add(p.getDate()));
		//Create a list of distinct dates
		List<String> dateList = dates.stream().distinct().collect(Collectors.toList());
		//extract get all activities
		data.stream().forEach(p -> activities.add(p.activity));
		//create a list of distinct activities
		List<String> activityList = activities.stream().distinct().collect(Collectors.toList());
				
		List<String> listing = new ArrayList<>();
		dateList.stream().forEach(
				cd -> activityList.stream().forEach(
				act -> listing.add(cd + " " + act + " " +
				data.stream().filter(p -> p.getDate().equals(cd) && p.activity.equals(act)).count()
				))
				);		
	
		String filename = "activities_per_day.txt";
		File logFile = new File(filename);
		BufferedWriter writer = new BufferedWriter(new FileWriter(logFile));		
		
		for(String s: listing)
		{
			writer.write(s + "\n");
		}
		writer.close();
	}
	
	private void activityDuration() throws IOException
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
		timeTakenTable = new HashMap<String, Long>();
		timeTakenKey = new ArrayList<String>();
		data.stream().forEach(
				p-> {
					try {
						timeTakenKey.add(p.activity + " " + p.startTime + " " + p.endTime);
						timeTakenTable.put(p.activity + " " + p.startTime + " " + p.endTime, 
								(Long)(format.parse(p.endTime).getTime()/1000 - format.parse(p.startTime).getTime()/1000));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				});
		
		String filename = "time_taken_to_perform.txt";
		File logFile = new File(filename);
		BufferedWriter writer = new BufferedWriter(new FileWriter(logFile));		
		
		
		for(String s : timeTakenKey)
		{
			writer.write(s + "\n " +
					timeTakenTable.get(s)/(3600) % 24 + ":" + timeTakenTable.get(s)/60 % 60  + ":" + timeTakenTable.get(s) % 60 
					+ "\n");
		}
		
		writer.close();
	}
	
	private void totalActivityDuration() throws IOException
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
		List<String> activities = new ArrayList<>();
		List<String> activityList = new ArrayList<>();
		//extract get all activities
		data.stream().forEach(p -> activities.add(p.activity));
		//create a list of distinct activities
		activityList = activities.stream().distinct().collect(Collectors.toList());
		HashMap<String, BigInteger> activitiesTotalDuration = new HashMap<String, BigInteger>();
		
		//Safety for no NullPointerExceptions
		for(String a : activityList)
		{
			activitiesTotalDuration.put(a, new BigInteger(0 + ""));
		}
		
		activityList.stream().forEach(
				a -> data.stream()
				.filter(p -> p.activity.equals(a))
				.forEach(p ->  {
					try {
						activitiesTotalDuration.put(a, 
								activitiesTotalDuration.get(a).add( new BigInteger(
								(format.parse(p.endTime).getTime()/1000 -
								format.parse(p.startTime).getTime()/1000) + ""))); 
					} catch (ParseException e) {
						e.printStackTrace();
					}
				})
				);
		
		String filename = "total_time_per_activity.txt";
		File logFile = new File(filename);
		BufferedWriter writer = new BufferedWriter(new FileWriter(logFile));		
		
		for(String a : activityList)
		{
			writer.write(a + " " + 
					activitiesTotalDuration.get(a).divide(new BigInteger(24*3600 + "")).abs() + " " +
					activitiesTotalDuration.get(a).divide(new BigInteger(3600 + "")).mod(new BigInteger(24 + "")) + ":"+
					activitiesTotalDuration.get(a).divide(new BigInteger(60 + "")).mod(new BigInteger(60 + "")) + ":" +
					activitiesTotalDuration.get(a).mod(new BigInteger(60 + "")) + "\n");
		}
		writer.close();
	}
	
	
	private void filter() throws IOException
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
		List<String> activities = new ArrayList<>();
		List<String> activityList = new ArrayList<>();
		//extract get all activities
		data.stream().forEach(p -> activities.add(p.activity));
		//create a list of distinct activities
		activityList = activities.stream().distinct().collect(Collectors.toList());
		HashMap<String, Integer> countedFiltered = new HashMap<>();
		List<MonitoredData> filteredActivities;
		
		filteredActivities = data.stream().filter(
				p -> 	{
					try {
						return (format.parse(p.endTime).getTime()/1000 -
								format.parse(p.startTime).getTime()/1000)/60 % 60 < 5;
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return false;
				}
				).collect(Collectors.toList());
		
		activityList.stream().forEach(
				a -> countedFiltered.put(a, (int) filteredActivities.stream().filter(p -> p.activity.equals(a)).count()));
	
		String filename = "filtered_activities.txt";
		File logFile = new File(filename);
		BufferedWriter writer = new BufferedWriter(new FileWriter(logFile));		
		
		
		for(String s : activityList)
		{
			float per = (float)countedFiltered.get(s) / map.get(s);
			if(per > 0.9f)
			{
				writer.write(s + "\n");
			}
		}
		
		writer.close();
	}
	
}






























