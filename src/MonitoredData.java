
public class MonitoredData {
	public String startTime, endTime, activity;
	
	MonitoredData(String s, String e, String a)
	{
		this.startTime = s;
		this.endTime = e;
		this.activity = a;
	}
	
	public void print()
	{
		System.out.println(this.startTime + " " + this.endTime + " " + this.activity);
	}
	
	public String getDate()
	{
		return startTime.substring(0, 10);
	}
}
